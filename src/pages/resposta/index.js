import { Button, Typography } from "@material-ui/core";
import { useHistory, useParams } from "react-router-dom";
import image from "../../imagem/undraw_Having_fun_re_vj4h.svg";
import { makeStyles } from "@material-ui/core/styles";

// estilzação dos componentes
//=====================================================
const useStyles = makeStyles((theme) => ({
  root: {
    backgroundColor: "lightBlue",
    width: "500px",
    padding: "20px 40px",
    border: "4px solid",
    borderRadius: "20px",
  },
  image: {
    width: "90%",
  },
}));

//função JSX
//================================================
function Resp() {
  const params = useParams();
  const history = useHistory();

  const handleClick = () => {
    history.push("/");
  };

  //return do JSX
  //=======================================
  const classes = useStyles();
  return (
    <div className={classes.root}>
      <Typography variant="h2" gutterBottom>
        Bem Vindo,
      </Typography>
      <p>{params.id}!!!</p>
      <img className={classes.image} src={image} alt="" />
      <Button
        type="submit"
        size="medium"
        variant="contained"
        color="primary"
        onClick={handleClick}
      >
        Voltar
      </Button>
    </div>
  );
}
export default Resp;
