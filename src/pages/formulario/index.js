import * as yup from "yup";
import { useForm } from "react-hook-form";
import { yupResolver } from "@hookform/resolvers/yup";
import { useHistory } from "react-router-dom";

import { TextField, Button } from "@material-ui/core";

import { makeStyles } from "@material-ui/core/styles";

// estilzação dos componentes
//=====================================================
const useStyles = makeStyles((theme) => ({
  root: {
    backgroundColor: "lightBlue",
    width: "260px",
    padding: "30px 20px",
    borderStyle: "solid",
    borderRadius: "20px",
    boxShadow: "5px 5px 5px lightgrey",
    borderWidth: "2px 0 0 2px",
  },
}));

//função JSX
//===================================================================
function Form() {
  const schema = yup.object().shape({
    name: yup
      .string()
      .required("*Campo Obrigatório")
      .matches(/^[a-záàâãéèêíïóôõöúçñ ]+$/i, "*Apenas letras"),
    email: yup.string().required("*Campo Obrigatório").email("*Email inválido"),
    password: yup
      .string()
      .required("*senha obrigatória")
      .min(8, "*mínimo 8 caracteres")
      .matches(
        /^(?=.*\d)(?=.*[a-z])(?=.*[A-Z])(?=.*[$*&@#])[0-9a-zA-Z$*&@#]{8,}$/,
        "*Num/Maiusc/Min/caracter especial"
      ),
    passwordConfirm: yup
      .string()
      .required("*senha errada")
      .oneOf([yup.ref("password")], "*senha diferente"),
  });

  const {
    register,
    handleSubmit,
    formState: { errors },
  } = useForm({ resolver: yupResolver(schema) });

  const history = useHistory();

  const onSubmitFunction = (data) => {
    console.log(data);
    history.push(`/resposta/${data.name}`);
  };

  //return do JSX
  //==================================================================
  const classes = useStyles();
  return (
    <div className={classes.root}>
      <form onSubmit={handleSubmit(onSubmitFunction)}>
        <div>
          <TextField
            id="outlined-basic"
            margin="normal"
            label="Nome"
            variant="outlined"
            size="small"
            {...register("name")}
            error={!!errors.name}
            helperText={errors.name?.message}
          />
        </div>
        <div>
          <TextField
            id="outlined-basic"
            margin="normal"
            label="Email"
            variant="outlined"
            size="small"
            {...register("email")}
            error={!!errors.email}
            helperText={errors.email?.message}
          />
        </div>
        <div>
          <TextField
            id="outlined-basic"
            margin="normal"
            label="Senha"
            variant="outlined"
            size="small"
            {...register("password")}
            error={!!errors.password}
            helperText={errors.password?.message}
          />
        </div>
        <div>
          <TextField
            id="outlined-basic"
            margin="normal"
            label="Confirmar Senha"
            variant="outlined"
            size="small"
            {...register("passwordConfirm")}
            error={!!errors.passwordConfirm}
            helperText={errors.passwordConfirm?.message}
          />
        </div>
        <div>
          <Button
            type="submit"
            size="medium"
            variant="contained"
            color="primary"
          >
            Cadastrar
          </Button>
        </div>
      </form>
    </div>
  );
}
export default Form;
