import "./App.css";
import { Switch, Route } from "react-router-dom";
import Form from "./pages/formulario";
import Resp from "./pages/resposta";

function App() {
  return (
    <div className="App">
      <header className="App-header">
        <Switch>
          <Route exact path="/">
            <Form />
          </Route>

          <Route exact path="/resposta/:id">
            <Resp />
          </Route>
        </Switch>
      </header>
    </div>
  );
}

export default App;
